﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace CalculationFormule
{
    public partial class CalculationFormuleForm : Form
    {
        private PostfixNotationExpression _postfixNotationExpression;

        public CalculationFormuleForm()
        {
            InitializeComponent();
            Load += OnLoad;
            buttonOK.Click += ButtonOkOnClick;
        }

        private void ButtonOkOnClick(object sender, EventArgs eventArgs)
        {
            var codeGenerator = new CodeGenerator();
            ControlOutputResult.Text = codeGenerator.GetValeFormGeneratedMethod(_postfixNotationExpression.Result(ControlInputFormule.Text).ToString(CultureInfo.InvariantCulture));
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            _postfixNotationExpression = new PostfixNotationExpression();
        }

        private void ControlInputFormuleValidated(object sender, EventArgs e)
        {
            errorProvider.SetError(labelInputFormula, string.Empty);
        }

        private void ControlInputFormuleValidating(object sender, CancelEventArgs e)
        {
            string errorMsg;
            if (!FormuleValidator.IsValidFormule(ControlInputFormule.Text, out errorMsg))
            {
                e.Cancel = true;
                ControlInputFormule.Select(0, ControlInputFormule.Text.Length);
                errorProvider.SetError(labelInputFormula, errorMsg);
            }
        }

        private void CalculationFormuleFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ControlInputFormule.CausesValidation)
            {
                return;
            }

            ControlInputFormule.CausesValidation = false;
            Close();
        }
    }
}
