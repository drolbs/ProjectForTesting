﻿
namespace CalculationFormule
{
    public static class Constants
    {
        public const string PatternOperandAtBeginning = @"^[+-/*//]";
        public const string PatternOperandAtEnd = @"[+-/*//]$";
        public const string PatternMissingNumberBetweenOperand = @"[+-/*//][+-/*//]";
        public const string PatternMissingBetweenBracketAndOperand = @"[(][+-/*//]";
        public const string PatternMissingBetweenOperandAndBracket = @"[+-/*//][)]";
        public const string PatternMissingBetweenSignAndBracket = @"[0-9][(]";
        public const string PatternMissingBetweenBracketAndSign = @"[)][0-9]";
        public const string PatternDoubleSign = @"[0-9] [0-9]";
        public const string PatternEmptyInBrackets = @"[(][)]";
        public const string TemplateChars = " 0123456789()+-*/";
        public const string MessageEmptyFormule = "Не задана формула";
        public const string MessageIncorrectChar = "Формула не должна содержать символа '{0}'";
        public const string MessageMissingBracket = "Пропущена открывающая или закрывающая скобка";
        public const string MessageIncorrectSequenceOfOperands = "Неверная последовательность операднов";
        public const string WhiteSpace = " ";
        public const string MyMthdName = "MyMethod";
        public const string MyDynamicAsm = "MyDynamicAsm";
        public const string MyDynamicAsmDll = "MyDynamicAsm.dll";
        public const string MyDynamicType = "MyDynamicType";
    }
}
