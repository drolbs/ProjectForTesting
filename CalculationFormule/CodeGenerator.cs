﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace CalculationFormule
{
    public class CodeGenerator
    {
        public CodeGenerator()
        {
            GenerateMethod();
        }

        private Type _myType;
        private const int Index = 0;

        private void AddMethodDynamically(TypeBuilder myTypeBld,
                                            string mthdName,
                                            Type[] mthdParams,
                                            Type returnType)
        {

            MethodBuilder myMthdBld = myTypeBld.DefineMethod(
                                                 mthdName,
                                                 MethodAttributes.Public |
                                                 MethodAttributes.Static,
                                                 returnType,
                                                 mthdParams);

            ILGenerator lout = myMthdBld.GetILGenerator();

            lout.Emit(OpCodes.Ldarg_S, 0);

            lout.Emit(OpCodes.Ret);
        }

        private void GenerateMethod()
        {
            AppDomain myDomain = AppDomain.CurrentDomain;
            var asmName = new AssemblyName {Name = Constants.MyDynamicAsm};
            AssemblyBuilder myAsmBuilder = myDomain.DefineDynamicAssembly(
                asmName,
                AssemblyBuilderAccess.RunAndSave);
            ModuleBuilder myModule = myAsmBuilder.DefineDynamicModule(Constants.MyDynamicAsm,
                Constants.MyDynamicAsmDll);
            TypeBuilder myTypeBld = myModule.DefineType(Constants.MyDynamicType,
                TypeAttributes.Public);
            var myMthdParams = new Type[1];
            
            myMthdParams[Index] = typeof(decimal);

            AddMethodDynamically(myTypeBld,
                                 Constants.MyMthdName,
                                 myMthdParams,
                                 typeof(decimal));

            _myType = myTypeBld.CreateType();

            myAsmBuilder.Save(Constants.MyDynamicAsmDll);
        }

        public string GetValeFormGeneratedMethod(string inputValue)
        {
            var inputValsList = new object[1];

            inputValsList[Index] = Convert.ToDecimal(inputValue);

            return _myType.InvokeMember(Constants.MyMthdName,
                BindingFlags.InvokeMethod | BindingFlags.Public |
                BindingFlags.Static,
                null,
                null,
                inputValsList).ToString();
        }
    }
}
