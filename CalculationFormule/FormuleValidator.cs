﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CalculationFormule
{
    public static class FormuleValidator
    {
        public static bool IsValidFormule(string formule, out string errorMessage)
        {
            if (formule.Length == 0)
            {
                errorMessage = Constants.MessageEmptyFormule;
                return false;
            }

            if (formule.Any(t => !Constants.TemplateChars.Contains(t)))
            {
                char incorrectChar = formule.First(t => !Constants.TemplateChars.Contains(t));
                errorMessage = string.Format(Constants.MessageIncorrectChar, incorrectChar);
                return false;
            }

            if (!CheckBrackets(formule))
            {
                errorMessage = Constants.MessageMissingBracket;
                return false;
            }

            if (CheckSymbol(formule))
            {
                errorMessage = Constants.MessageIncorrectSequenceOfOperands;
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        private static bool CheckBrackets(string checkString)
        {
            var braceStack = new Stack<char>();

            foreach (var chr in checkString)
            {
                if (chr == '(')
                {
                    braceStack.Push(chr);
                    continue;
                }

                if (chr != ')') continue;

                char brace;

                if (braceStack.Count > 0)
                    brace = braceStack.Pop();
                else
                    return false;

                switch (brace)
                {
                    case '(':
                        if (chr != ')') return false;
                        break;
                }
            }

            return braceStack.Count == 0;
        }

        private static bool CheckSymbol(string checkString)
        {
            var checkStringWithoutSpace = checkString.Replace(Constants.WhiteSpace, string.Empty);
            return Regex.IsMatch(checkStringWithoutSpace, Constants.PatternOperandAtBeginning, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternOperandAtEnd, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternMissingNumberBetweenOperand, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternMissingBetweenBracketAndOperand, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternMissingBetweenOperandAndBracket, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternMissingBetweenBracketAndSign, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternMissingBetweenSignAndBracket, RegexOptions.None) ||
                   Regex.IsMatch(checkString, Constants.PatternDoubleSign, RegexOptions.None) ||
                   Regex.IsMatch(checkStringWithoutSpace, Constants.PatternEmptyInBrackets, RegexOptions.None);
        }
    }
}
