﻿open System
open System.IO
open System.Text.RegularExpressions
open System.Collections.Generic
         
let is_operator c =
    match c with
        | "+" | "-" | "*" | "/"  -> true
        | _ -> false
 
let calculate left operator right =
    match operator with
        | "+" -> left + right
        | "-" -> left - right
        | "*" -> left * right
        | "/" -> left / right
        | _ -> failwith "Unknown operator"
 
let proc (item:string) (stack : System.Collections.Generic.Stack<double>) =
    if is_operator item then
        let r = stack.Pop()
        let l = stack.Pop()
        let result = calculate l item r
        stack.Push result
    else
        stack.Push(System.Convert.ToDouble(item))
 
let evaluate (expression:string) =
    let stack = new System.Collections.Generic.Stack<double>()
    let items = expression.Split([|' ' |])
    Array.iter (fun i -> proc i stack) items
    stack.Pop()

[<EntryPoint>]
let main argv = 
    printf "%A" (evaluate ("1 2 + 4 / 5 * 6 -"))
    Console.ReadKey()
    printfn "%A" argv
    0 // return an integer exit code
