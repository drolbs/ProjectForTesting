﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using CalculationFormule;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class PostfixNotationExpressionTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            string formule1 = "(2+3)-1*(2+10/5)";
            string formule2 = "(2+8/5)";
            decimal resultFormuleInt = 1;
            decimal resultFormuleDecimal = (decimal)3.6;
            decimal result;

            PostfixNotationExpression expression = new PostfixNotationExpression();

            result = expression.Result(formule1);
            Assert.AreEqual(resultFormuleInt, result);
            
            result = expression.Result(formule2);
            Assert.AreEqual(resultFormuleDecimal, result);
        }
    }
}
