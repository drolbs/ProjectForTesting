﻿using System.Collections.Generic;
using CalculationFormule;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    /// <summary>
    /// Summary description for FormuleValidatorTest
    /// </summary>
    [TestClass]
    public class FormuleValidatorTest
    {
        [TestMethod]
        public void IsValidFormuleTest()
        {
            IList<string> valuesIncorrectFormule = new List<string>()
                {
                    "",
                    "(2+3",
                    "(2+3",
                    "2+3)",
                    "()2+3",
                    "(+2+3",
                    "+(2+3)",
                    "(2+3)+",
                    "(2+3+)",
                    "(2++3)",
                    "(2+3)*2(3+4)",
                    "(2+3)2*(3+4)",
                    "(2 2+3)",
                };

            IList<string> valuesCorrectFormule = new List<string>()
                {
                    " ",
                    "(2+3)*4+(4/5)-2",
                };

            string result;
            foreach (var formule in valuesIncorrectFormule)
            {
                Assert.IsFalse(FormuleValidator.IsValidFormule(formule, out result));                
            }

            foreach (var formule in valuesCorrectFormule)
            {
                Assert.IsTrue(FormuleValidator.IsValidFormule(formule, out result));                
            }
        }
    }
}
