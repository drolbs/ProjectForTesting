﻿using CalculationFormule;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class CodeGeneratorTest
    {
        [TestMethod]
        public void GetValeFormGeneratedMethodTest()
        {
            string valueTestInt = "2";
            string valueTestDecimal = "2,2";
            string result;

            var codeGenerator = new CodeGenerator();

            result = codeGenerator.GetValeFormGeneratedMethod(valueTestInt);
            Assert.AreEqual(valueTestInt, result);
            result = codeGenerator.GetValeFormGeneratedMethod(valueTestDecimal);
            Assert.AreEqual(valueTestDecimal, result);
        }
    }
}
